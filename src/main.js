document.addEventListener("DOMContentLoaded", function () {
    
  const main = document.querySelector("#app-container");
  const footer = document.querySelector(".app-footer");
  const section = document.createElement("section");
  const heading = document.createElement("h2");
  const subTitle = document.createElement("h3");
  const form = document.createElement("form");
  const email = document.createElement("input");
  const btn = document.createElement("input");

  section.classList.add("app-section--join-program");
  heading.classList.add("app-title");
  subTitle.classList.add("app-subtitle");
  form.classList.add("form-join");
  email.classList.add("email");
  btn.classList.add("btn-subscribe");

  //Title
  heading.textContent = "Join Our Program";
  heading.style.color = "#fff";
  heading.style.marginTop = "0";

  //Subtitle
  subTitle.textContent = `Sed do eiusmod tempor incididunt 
                        ut labore et dolore magna aliqua.`;
  subTitle.style.maxWidth = "400px";
  subTitle.style.margin = "0 auto";
  subTitle.style.color = "rgba(255, 255, 255, 0.7)";

  //Form
  form.style.marginTop = "63px";
  form.textAlign = "center";


  //Email input
  form.appendChild(email);
  email.setAttribute("placeholder", "Email");
  email.setAttribute("type", "email");
  email.setAttribute("name", "emailaddress");
//   email.style.width = "400px";
  email.style.padding = "10px 20px";
  email.style.background = "rgba(255, 255, 255, 0.15)";
  email.style.border = "none";
  email.style.color = "#fff";
//   email.placeholder.color = '#fff';
  email.style.outline = "none";
  email.setAttribute('required', 'required')

  // Btn input
  form.appendChild(btn);
  btn.setAttribute("type", "submit");
  btn.setAttribute("name", "submit");
  btn.setAttribute("value", "Subscribe");
  btn.style.font = "400 14px 'Oswald', sans-serif";
  btn.style.margin = "0 30px";
  btn.style.padding = "5px 15px";
  btn.style.color = "#fff";
  btn.style.backgroundColor = "#55C2D8";
  btn.style.borderRadius = "18px";
  btn.style.border = "none";
  btn.style.textTransform = "uppercase";
  btn.style.lineHeight = "26px";
  btn.style.textAlign = "center";
  btn.style.letterSpacing = "0.1em";
  btn.style.cursor = "pointer";

  // Section style
  section.style.textAlign = "center";
  section.style.display = "flex";
  section.style.flexDirection = "column";
  section.style.alignItems = "center";
  section.style.background =
    "linear-gradient(to top, rgba(5, 17, 18, .5), rgba(5, 17, 18, .5)), url(assets/images/section-join-program.jpg) no-repeat";
  section.style.backgroundSize = "cover";
  section.style.maxWidth = "100%";
  section.style.padding = "87px 30px 100px 30px";
  section.style.display = "block";

  main.appendChild(section);
  footer.insertAdjacentElement("beforebegin", section);
  section.appendChild(heading);
  heading.insertAdjacentElement("afterend", subTitle);
  section.appendChild(form);

  btn.addEventListener('click', function(e){
    e.preventDefault()
    console.log(email.value)
})

if (matchMedia) {
    const mq = window.matchMedia("(max-width: 500px)");
    mq.addListener(WidthChange);
    WidthChange(mq);
    }
    
    // media query change
    function WidthChange(mq) {
    if (mq.matches) {
        heading.style.fontSize = '48px';
        subTitle.style.lineHeight = "32px";
        subTitle.style.width = "350px";
        email.style.width = "90%";
        form.style.marginTop = "55px";
        btn.style.marginTop = '42px';
        section.style.backgroundPosition = '24%';
        section.style.backgroundSize = 'cover'
    } else {
    // window width is more than 500px
    subTitle.style.width = "400px";
    email.style.width = "400px";
    form.style.marginTop = "63px";
    btn.style.marginTop = '0px';
    }
    
    }

});